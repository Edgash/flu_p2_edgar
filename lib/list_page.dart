
import 'dart:convert';

import 'package:flu_p2_edgar/model/product_data.dart';
import 'package:flu_p2_edgar/product_details_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListPage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Productos"),
        backgroundColor: Colors.deepPurple,
      ),
      body: FutureWidget(),
    );
  }
}

class FutureWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Center(
      child: FutureBuilder(
        future: DefaultAssetBundle.of(context).loadString('assets/json/products.json'),
        builder: (context, AsyncSnapshot<String> snapshot){
          if(snapshot.hasData){

            Map<String, dynamic> data = json.decode(snapshot.data);
            var products = Products.fromJson(data);

            return ListView.builder(
            itemCount: products.products.length,
            itemBuilder: (context, i) => ListTile(
              trailing: Icon(Icons.arrow_forward_ios),
              title: Text("${products.products[i].name}"),
              subtitle: Text("${products.products[i].price}"),
              leading: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 44,
                  minHeight: 44,
                  maxWidth: 44,
                  maxHeight: 44,
                ),
                child: Image.asset("${products.products[i].image}", fit: BoxFit.cover),
              ),
              onTap: () {
                var route = MaterialPageRoute(
                    builder: (context) => ProductDetail(),
                );
                Navigator.of(context).push(route);
              },
            ),
          );
          }else{
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }
}
