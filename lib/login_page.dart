
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'list_page.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Row(children: [
              Container(width: 100, height: 50, child: const Text("Login",
                style: TextStyle(
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                ),
              ), margin: const EdgeInsets.only(left: 20.0, top: 30),),
              Spacer(),
              Container(width: 40, height: 30, decoration: BoxDecoration(
                  color: Colors.deepPurpleAccent,
                  shape: BoxShape.circle,
                  boxShadow: [BoxShadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    spreadRadius: 5.0,
                    offset: Offset(0.0, 0.0,),),]), margin: const EdgeInsets.only(top: 20)),
              Container(width: 70, height: 70, decoration: BoxDecoration(
                  color: Colors.deepPurpleAccent,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 10.0, // soften the shadow
                      spreadRadius: 5.0, //extend the shadow
                      offset: Offset(
                        0.0, // Move to right 10  horizontally
                        0.0, // Move to bottom 10 Vertically
                      ),
                    ),
                  ]), margin: const EdgeInsets.only(right: 20.0, top: 20, left: 5),),]
            ),
            Row(children: [
              Container(width: 340, height: 210, decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [BoxShadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    spreadRadius: 5.0,
                    offset: Offset(0.0, 0.0,),),]
              ),margin: const EdgeInsets.only(left: 27.0, top: 100),
                child: Column(
                  children: [
                    Container(child: TextField(
                      decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.person,color: Colors.deepPurple,),
                          hintText: 'Username'),
                    ), margin: const EdgeInsets.only(left: 20.0, top: 60, right: 20)),
                    Container(child: TextField(
                      decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.vpn_key,color: Colors.deepPurple,),
                          hintText: 'Password'),
                    ), margin: const EdgeInsets.only(left: 20.0, top: 15, right: 20))
                  ],),
              )]),
            GestureDetector(
              child: Container(
                width: 200,
                height: 40,
                alignment: Alignment.center,
                child: Text("LOGIN",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                  ), ),
                margin: const EdgeInsets.only(top: 180, left: 200), decoration: BoxDecoration(
                  boxShadow: [BoxShadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    spreadRadius: 5.0,
                    offset: Offset(0.0, 0.0,),),],
                  borderRadius: BorderRadius.only(bottomLeft:Radius.circular(20), topLeft: Radius.circular(20)),
                  color: Colors.deepPurple
              ),
              ),
              onTap: (){
                var route = MaterialPageRoute(builder: (context) => ListPage(),);
                Navigator.of(context).push(route);
              },
            )
          ],
        ),
      ),
    );
  }
}